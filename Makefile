EDITOR = geany

clean:
	# Remove files not in source control
	find . -type f -name "*.retry" -delete
	find . -type f -name "*.orig" -delete

open_all:
	${EDITOR} .gitignore hosts Makefile README.md site.yml
	${EDITOR} ./roles/debian/files/adduser.conf
	${EDITOR} ./roles/debian/files/apt_periodic
	${EDITOR} ./roles/debian/files/gitconfig
	${EDITOR} ./roles/debian/files/vimrc
	${EDITOR} ./roles/debian/files/zsh/*
	${EDITOR} ./roles/debian/tasks/*.yml
